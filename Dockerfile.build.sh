#!/bin/sh
model=$1

first_docker_build_args="--pull"

tag="$CI_REGISTRY_IMAGE/$model$CI_IMAGE_TAIL"
sed "s/T2T_MODEL/$model/g" Dockerfile.template | sed "s#BASEIMG#$CI_REGISTRY_IMAGE/base$CI_IMAGE_TAIL#g" | docker build $first_docker_build_args -t "$tag" -
first_docker_build_args=
docker push "$tag"
