#!/usr/bin/env python3
import argparse
import json
import re
import sys
import os

import aiohttp
import aiohttp.web

from sentence_splitter import split_text_into_sentences

import tensorflow as tf
from tensor2tensor.serving import serving_utils
from tensor2tensor.utils import registry
from tensor2tensor.utils import usr_dir


class TFServingElgAdapter:
    SUPPORTED_PROBLEMS = {
        "en-cs": "translate_encs",
        "cs-en": "translate_encs",
        "en-fr": "translate_enfr",
        "fr-en": "translate_enfr",
        "en-de": "translate_ende",
        "de-en": "translate_deen",
        "en-pl": "translate_enpl",
        "pl-en": "translate_plen",
        "en-ru": "translate_enru",
        "ru-en": "translate_ruen",
        "en-hi": "translate_enhi"
    }

    class ProcessingError(Exception):
        def __init__(self, code, text, *params):
            self.code = code
            self.text = text
            self.params = params

        @staticmethod
        def InternalError(text):
            return TFServingElgAdapter.ProcessingError("elg.service.internalError", "Internal error during processing: {0}", text)

        @staticmethod
        def InvalidRequest():
            return TFServingElgAdapter.ProcessingError("elg.request.invalid", "Invalid request message")

        @staticmethod
        def TooLarge():
            return TFServingElgAdapter.ProcessingError("elg.request.too.large", "Request size too large")

        @staticmethod
        def UnsupportedMime(mime):
            return TFServingElgAdapter.ProcessingError("elg.request.text.mimeType.unsupported", "MIME type {0} not supported by this service", mime)

        @staticmethod
        def UnsupportedType(request_type):
            return TFServingElgAdapter.ProcessingError("elg.request.type.unsupported", "Request type {0} not supported by this service", request_type)

        @staticmethod
        def UnsupportedParams(params):
            return TFServingElgAdapter.ProcessingError("elg.request.params.unsupported", "Request params {0} not supported by this service", params)

        @staticmethod
        def UnsupportedInputFormat(input_format):
            return TFServingElgAdapter.ProcessingError("elg.request.params.input_format.unsupported", "Request params.inputFormat {0} not supported by this service", input_format)


    def __init__(self,
                 host,
                 port,
                 max_request_size,
                 session,
                 models,
                 t2t_usr_dir="/translation/usr_dir",
                 t2t_data_dir="/translation/data_dir"):
        self.server = "{}:{}".format(host, port)
        self.max_request_size = max_request_size
        self.session = session

        usr_dir.import_usr_dir(t2t_usr_dir)

        self.problems = {
            model: registry.problem(self.SUPPORTED_PROBLEMS[model])
            for model in models}
        hparams = tf.contrib.training.HParams(
            data_dir=os.path.expanduser(t2t_data_dir))
        for _, problem in self.problems.items():
            problem.get_hparams(hparams)

    def _error_message(self, message):
        return {
            "failure": {
                "errors": [{
                    "code": message.code,
                    "text": message.text,
                    "params": message.params,
                }]
            }
        }

    def _translation_message(self, texts, structured=False):
        return {
            "response": {
                "type":"texts",
                "texts": texts
            }
        }

    async def _parse_elg_request(self, request):
        try:
            body = await request.text()
            if request.content_type == "text/plain":
                return [{ "content": body }], "sentence"
            elif request.content_type == "application/json":
                try:
                    request = json.loads(body)
                except json.JSONDecodeError:
                    raise self.ProcessingError.InvalidRequest()

                if not isinstance(request, dict):
                    raise self.ProcessingError.InvalidRequest()

                if request.get("mimeType", "text/plain") != "text/plain":
                    raise self.ProcessingError.UnsupportedMime(request["mimeType"])

                params = request.get("params", {"inputFormat": "sentence"})
                if not isinstance(params, dict):
                    raise self.ProcessingError.UnsupportedParams(params)

                input_format = params.get("inputFormat", "sentence")
                if input_format not in ["sentence", "paragraph", "wrappedText"]:
                    raise self.ProcessingError.UnsupportedInputFormat(input_format)

                request_type = request.get("type", "<missing>")
                if request_type == "text":
                    content = [{ "content": request.get("content", "") }]
                elif request_type == "structuredText":
                    content = request.get("texts", [])
                else:
                    raise self.ProcessingError.UnsupportedType(request_type)

                return content, input_format
            else:
                raise self.ProcessingError.UnsupportedMime(request.content_type)
        except aiohttp.web.HTTPRequestEntityTooLarge:
            raise self.ProcessingError.TooLarge()

    async def _run_tfquery(self, content, model, input_format):
        # Prepare the grpc request
        # NOTE: Can we make this asynchronous?
        request_fn = serving_utils.make_grpc_request_fn(
            servable_name=model,
            server=self.server,
            timeout_secs=100)

        # TODO: optimize structuredText processing

        # We process the structure recursively
        def _translate(c):
            out = []
            for item in c:
                text = item.get("content", None)
                if item.get("mimeType", "text/plain") != "text/plain":
                    raise self.ProcessingError.UnsupportedMime(request["mimeType"])
                if text is not None:
                    # TODO: properly handle various values of inputFormat
                    if input_format != "sentence":
                        text = split_text_into_sentences(text=text, language=model.split("-")[0])
                    else:
                        text = [text]
                    translated = serving_utils.predict(text, self.problems[model], request_fn)
                    translated = [x[0] for x in translated]
                    out.append({ "content": "\n".join(translated) })
                else:
                    text = item.get("texts", [])
                    out.append({ "texts": _translate(text) })
            return out

        return _translate(content)

    def _json_response(self, json):
        return aiohttp.web.json_response(json, headers={"Access-Control-Allow-Origin": "*"})

    async def process_elg_request(self, request):
        model = request.match_info.get("model", "")
        try:
            content, input_format = await self._parse_elg_request(request)
            output = await self._run_tfquery(content, model, input_format)
            return self._json_response(self._translation_message(output))

        except self.ProcessingError as e:
            return self._json_response(self._error_message(e))
        except:
            import traceback
            e_name, e_val, e_tb = sys.exc_info()
            traceback.print_exception(e_name, e_val, e_tb, file=open("errors.log", "a"))
            return self._json_response(self._error_message(
                self.ProcessingError.InternalError("Internal service error")))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--max_request_size", default=1000000, type=int, help="Maximum request size")
    parser.add_argument("--port", default=8080, type=int, help="Port where to start the REST server")
    parser.add_argument("--tfserving_host", default="localhost", type=str, help="Host of running TF Model Server")
    parser.add_argument("--tfserving_port", default=8002, type=int, help="Port of running TF Model Server")
    parser.add_argument("--models", type=str, nargs="+", required=True, help="Which T2T models to register")
    args = parser.parse_args()

    async def server(args):
        session = aiohttp.ClientSession()
        adapter = TFServingElgAdapter(args.tfserving_host, args.tfserving_port, args.max_request_size, session, args.models)

        # Allow triple the limit in getting the request -- it might use %XY encoding.
        server = aiohttp.web.Application(client_max_size=3 * args.max_request_size)
        server.add_routes([aiohttp.web.post("/translation/{model}", adapter.process_elg_request)])
        server.on_cleanup.append(lambda app: session.close())

        return server

    aiohttp.web.run_app(server(args), port=args.port)
